
let ptrT = newType("pointer")

globalModule("auro.machine.mem"):
  self["pointer"] = ptrT

  self.addfn("null", [], [ptrT]):
    args.ret(Value(kind: ptrV, pt: cast[pointer](0)))

  self.addfn("alloc", [intT], [ptrT]):
    args.ret(Value(kind: ptrV, pt: alloc(args[0].i)))

  self.addfn("read", [ptrT], [intT]):
    let pt = cast[ptr uint8](args[0].pt)
    let v = cast[int](pt[])
    args.ret(Value(kind: intV, i: v))

  self.addfn("write", [ptrT, intT], []):
    let pt = cast[ptr uint8](args[0].pt)
    pt[] = cast[uint8](args[1].i)

  self.addfn("offset", [ptrT, intT], [ptrT]):
    var pint = cast[uint](args[0].pt)
    pint += cast[uint](args[1].i)
    let pt = cast[pointer](pint)
    args.ret(Value(kind: ptrV, pt: pt))

  self.addfn("size", [], [intT]):
    args.ret(Value(kind: intV, i: sizeof(pointer)))



  self.addfn("allocany", [anyT], [ptrT]):
    let pt = alloc(sizeof(Value))
    cast[ptr Value](pt)[] = args[0]
    args.ret(Value(kind: ptrV, pt: pt))

  self.addfn("readany", [ptrT], [anyT]):
    let val = cast[ptr Value](args[0].pt)[]
    args.ret(val)


  self.addfn("write\x1dpointer", [ptrT, ptrT], []):
    let pt = cast[ptr pointer](args[0].pt)
    pt[] = args[1].pt

  self.addfn("read\x1dpointer", [ptrT], [ptrT]):
    let pt = cast[ptr pointer](args[0].pt)
    args.ret(Value(kind: ptrV, pt: pt[]))



globalModule("auro.machine.primitive"):
  proc make_type (name: string, base_type: Type) =
    let tp = newType(name)
    self[name] = tp

    self.addfn(name & "\x1dnew", [base_type], [tp]):
      discard
    
    self.addfn(name & "\x1dget", [tp], [base_type]):
      discard

  for name in ["u8","u16","u32","u64","i8","i16","i32","i64"]:
    make_type(name, intT)

  make_type("f32", fltT)
  make_type("f64", fltT)


  var mem_module = findModule("auro\x1fmachine\x1fmem")

  template memops (name: string, base: untyped, mykind: typed, fieldname: untyped):untyped =
    let tp = self[name].t

    mem_module.addfn("write\x1d" & name, [ptrT, tp], []):
      let pt = cast[ptr base](args[0].pt)
      pt[] = cast[base](args[1].fieldname)

    mem_module.addfn("read\x1d" & name, [ptrT], [tp]):
      let pt = cast[ptr base](args[0].pt)
      args.ret(Value(kind: mykind, fieldname: pt[]))

  memops("f64", float64, fltV, f)