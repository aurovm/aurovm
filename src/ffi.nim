
import machine

import dynlib
import libffi

import tables

var ffi_modules = initTable[string, Module](32)

var self = SimpleModule("auro\x1fffi", [])

type Type = machine.Type
type FfiType = libffi.Type

template addfn* (
  mymod: Module,
  myname: string,
  myins: openArray[Type],
  myouts: openArray[Type],
  body: untyped
) = 
  mymod.items.add(FunctionItem(myname, Function(
    name: myname,
    sig: Signature(ins: @myins, outs: @myouts),
    kind: procF,
    prc: proc (myargs: var seq[Value]) =
      var args {.inject.}: seq[Value]
      args.shallowCopy(myargs)
      body
  )))

let strT = findModule("auro\x1fstring")["string"].t
let fltT = findModule("auro\x1ffloat")["float"].t
let intT = findModule("auro\x1fint")["int"].t
let ptrT = findModule("auro\x1fmachine\x1fmem")["pointer"].t

let primitive_module = findModule("auro\x1fmachine\x1fprimitive")

var ffi_type_table = initTable[Type, ptr libffi.Type](16)

let primitives = {
  "u8": type_uint8.addr,
  "u16": type_uint16.addr,
  "u32": type_uint32.addr,
  "u64": type_uint64.addr,
  "i8": type_sint8.addr,
  "i16": type_sint16.addr,
  "i32": type_sint32.addr,
  "i64": type_sint64.addr,
  "f32": type_float.addr,
  "f64": type_double.addr,
}

for pair in primitives:
  let (name, ffi_type) = pair
  let tp = primitive_module[name].t;
  ffi_type_table[tp] = ffi_type

ffi_type_table[ptrT] = type_pointer.addr

type

  # Object that defines an FFI Function.
  # Manually allocated on the heap, so that it's not garbage collected.
  FnDef = object
    name: string
    cif: TCif
    params: ParamList
    sig: Signature
    proc_sym: pointer

  # Object that's used for the status of a specific call.
  # Manually allocated on the heap also.
  CallObj = object
    # This is the list of pointers to each argument
    args: ArgList
    # This is where the arguments are actually stored
    raw: array[0..20, uint64]
    # This is where to store the return of the function
    ret: uint64


proc get_type_ptr (item: Item): ptr FfiType =
  if item.kind != tItem:
    raise newException(Exception, "module argument " & item.name.main & " is not a type")
  if not ffi_type_table.has_key(item.t):
    raise newException(Exception, "module argument " & item.name.main & " is not a valid ffi type")
  ffi_type_table[item.t]


proc value_to_raw (val: Value, raw_val: pointer, tp: Type) =
  template assign (m_t: untyped, m_v: untyped):untyped =
    (cast[ptr m_t](raw_val))[] = cast[m_t](m_v)

  let ffi_type = ffi_type_table[tp]

  if ffi_type == type_sint8.addr:
    assign(int8, val.i)
  elif ffi_type == type_sint16.addr:
    assign(int16, val.i)
  elif ffi_type == type_sint32.addr:
    assign(int32, val.i)
  elif ffi_type == type_sint64.addr:
    assign(int64, val.i)
  elif ffi_type == type_uint8.addr:
    assign(uint8, val.i)
  elif ffi_type == type_uint16.addr:
    assign(uint16, val.i)
  elif ffi_type == type_uint32.addr:
    assign(uint32, val.i)
  elif ffi_type == type_uint64.addr:
    assign(uint64, val.i)
  elif ffi_type == type_pointer.addr:
    assign(pointer, val.pt)
  else:
    raise newException(Exception, "Unsupported type: " & $tp)


proc raw_to_value (raw: pointer, tp: Type): Value =
  let ffi_type = ffi_type_table[tp]

  if ffi_type == type_sint8.addr:
    result = Value(kind: intV, i: cast[ptr int8](raw)[])
  elif ffi_type == type_sint16.addr:
    result = Value(kind: intV, i: cast[ptr int16](raw)[])
  elif ffi_type == type_sint32.addr:
    result = Value(kind: intV, i: cast[ptr int32](raw)[])
  elif ffi_type == type_sint64.addr:
    result = Value(kind: intV, i: int(cast[ptr int64](raw)[]))
  elif ffi_type == type_uint8.addr:
    result = Value(kind: intV, i: int(cast[ptr uint8](raw)[]))
  elif ffi_type == type_uint16.addr:
    result = Value(kind: intV, i: int(cast[ptr uint16](raw)[]))
  elif ffi_type == type_uint32.addr:
    result = Value(kind: intV, i: int(cast[ptr uint32](raw)[]))
  elif ffi_type == type_uint64.addr:
    result = Value(kind: intV, i: int(cast[ptr uint64](raw)[]))
  elif ffi_type == type_pointer.addr:
    result = Value(kind: ptrV, pt: cast[ptr pointer](raw)[])
  else:
    raise newException(Exception, "Unsupported type: " & $tp)

proc import_proc (argument: Module): Module =
  var argitem = argument["0"]

  if argitem.kind != fItem:
    raise newException(Exception, "argument 0 for auro.ffi is not a function")

  let arg_f = argitem.f
  if arg_f.sig != Signature(ins: @[], outs: @[strT]):
    raise newException(Exception, "function argument 0 of auro.ffi must accept nothing and return a string")

  let lib_name = arg_f.run(@[])[0].s

  if ffi_modules.hasKey(lib_name):
    return ffi_modules[lib_name]

  let is_path = lib_name[0] == '.' and lib_name[1] == '/' or lib_name[0] == '/'
  let lib_path = if is_path: lib_name else: "/usr/lib/" & lib_name & ".so"

  let dll = loadLib(lib_path, false)
  if dll == nil:
    raise newException(Exception, "cannot load library " & lib_path)

  let basename = "ffi(" & lib_name & ")"

  result = SimpleModule(basename, [])
      

  proc get_builder (argument: Module): Module =
    let fnameitem = argument["name"]

    if fnameitem.kind != fItem:
      raise newException(Exception, "argument 0 for auro.ffi is not a function")

    let arg_f = fnameitem.f
    if arg_f.sig != Signature(ins: @[], outs: @[strT]):
      raise newException(Exception, "function argument 0 of auro.ffi must accept nothing and return a string")

    let fname = arg_f.run(@[])[0].s

    let proc_sym = dll.symAddr(fname)
    if proc_sym == nil:
      raise newException(Exception, "function " & fname & " not found in " & lib_name)

    # everything of the fucntion has to be allocated manually.
    # If handled by the gc, the compiler doesn't know the values are not used
    # again after this function, so they would get freed before they are used
    var fndef: ptr FnDef = cast[ptr FnDef](alloc0(sizeof(FnDef)))
    fndef.name = libname & "/" & fname
    fndef.sig = Signature()
    fndef.proc_sym = proc_sym

    var param_count: cuint = 0

    var out_type: ptr libffi.Type

    let out_item = argument["out"]
    if out_item.kind == nilItem:
      out_type = type_void.addr
    else:
      out_type = get_type_ptr(out_item)
      fndef.sig.outs.add(out_item.t)


    var in_item = argument["in" & $param_count]
    while in_item.kind != nilItem:
      fndef.params[param_count] = get_type_ptr(in_item)

      fndef.sig.ins.add(in_item.t)

      param_count += 1
      in_item = argument["in" & $param_count]

    if fndef.cif.prep_cif(DEFAULT_ABI, param_count, out_type, fndef.params) != Ok:
      raise newException(Exception, "could not prepare function " & libname & "." & fname)

    result = SimpleModule(libname & "." & fname, [])
    result.addfn("", fndef.sig.ins, fndef.sig.outs):

      var call_obj: ptr CallObj = cast[ptr CallObj](alloc0(sizeof(CallObj)))

      for i in 0 .. fndef.sig.ins.high:

        template assign (m_t: untyped, m_v: untyped):untyped =
          (cast[ptr m_t](callobj.raw[i].addr))[] = cast[m_t](m_v)

        var val = args[i]
        let ffi_type = ffi_type_table[fndef.sig.ins[i]]

        if ffi_type == type_sint8.addr:
          assign(int8, val.i)
        elif ffi_type == type_sint16.addr:
          assign(int16, val.i)
        elif ffi_type == type_sint32.addr:
          assign(int32, val.i)
        elif ffi_type == type_sint64.addr:
          assign(int64, val.i)
        elif ffi_type == type_uint8.addr:
          assign(uint8, val.i)
        elif ffi_type == type_uint16.addr:
          assign(uint16, val.i)
        elif ffi_type == type_uint32.addr:
          assign(uint32, val.i)
        elif ffi_type == type_uint64.addr:
          assign(uint64, val.i)
        elif ffi_type == type_float.addr:
          assign(float32, val.f)
        elif ffi_type == type_double.addr:
          assign(float64, val.f)
        elif ffi_type == type_pointer.addr:
          assign(pointer, val.pt)
        else:
          raise newException(Exception, "Unsupported type: " & $fndef.sig.ins[i])

        callobj.args[i] = callobj.raw[i].addr

      fndef.cif.call(fndef.proc_sym, callobj.ret.addr, callobj.args)

      if fndef.sig.outs.len > 0:
        args[0] = raw_to_value(callobj.ret.addr, fndef.sig.outs[0])


  let get_functor = CustomModule(libname & ".get", nil, get_builder)
  result.items.add(ModuleItem("get", get_functor))
      
  ffi_modules[lib_name] = result


type InnerClosure = proc (ret: pointer, args: UncheckedArray[pointer])

type
  # Context that is passed to a closure to know how to interact with the base auro function
  Ctx = object
    sig: Signature
    f: Function
  # Object that describes a closure.
  # Allocated manually on the heap to avoid garbage collection issues
  ClosureDef = object
    # Ctx object
    ctx: Ctx
    cif: TCif
    # direct pointer to the raw function
    fptr: pointer
    params: ParamList
    # Thsi type is ffi specific and required by ffi
    ffi_closure: ptr libffi.Closure

proc raw_closure_proc(cif: var TCif, ret: pointer, args: UncheckedArray[pointer], user_data: pointer) {.cdecl} =
  let ctx = cast[ptr Ctx](user_data)[]
  var ins = newSeq[Value]()

  for i in 0 .. ctx.sig.ins.high:
    ins.add(raw_to_value(args[i], ctx.sig.ins[i]))

  var outs = ctx.f.run(ins)

  if ctx.sig.outs.len > 0:
    value_to_raw(outs[0], ret, ctx.sig.outs[0])


proc closure_proc (argument: Module): Module =

  var def: ptr ClosureDef = cast[ptr ClosureDef](alloc0(sizeof(ClosureDef)))

  # params has to be allocated manually.
  # If handled by the gc, the compiler doesn't know the value is not used
  # again after this function, so it gets freed before it's used

  var param_count: cuint = 0
  var out_type: ptr libffi.Type

  # Build both auro signature and libffi argument list

  var sig = Signature()

  let out_item = argument["out"]
  if out_item.kind == nilItem:
    out_type = type_void.addr
  else:
    out_type = get_type_ptr(out_item)
    sig.outs.add(out_item.t)

  var in_item = argument["in" & $param_count]
  while in_item.kind != nilItem:
    def.params[param_count] = get_type_ptr(in_item)

    sig.ins.add(in_item.t)

    param_count += 1
    in_item = argument["in" & $param_count]

  let basename = "auro.ffi.function(" & sig.name & ")"


  # Extract function argument

  let item = argument["function"]
  if item.kind != fItem:
    raise newException(Exception, "Argument `function` is not a function")
  let f = item.f
  if f.sig != sig:
    raise newException(Exception, "Incorrect function signature")

  # Allocate user_data (Context) on the heap, so that it can read by a
  # function that outlives this function
  def.ctx = Ctx(sig: sig, f: f)


  # Allocate and initialize libffi closure pointer

  def.ffi_closure = closure_alloc(def.fptr)

  if def.cif.prep_cif(DEFAULT_ABI, param_count, out_type, def.params) != Ok:
    raise newException(Exception, "could not prepare function " & basename)

  if OK != prep_closure_loc(def.ffi_closure, def.cif, raw_closure_proc, def.ctx.addr, def.fptr):
    echo "Something went wrong initializing the closure"
    quit 1


  let cnsf = Function(
    name: basename,
    sig: Signature(ins: @[], outs: @[ptrT]),
    kind: constF,
    value: Value(kind: ptrV, pt: def.fptr)
  )

  SimpleModule(basename, [FunctionItem("", cnsf)])


self.items.add(ModuleItem("import", CustomModule("import", nil, import_proc)))
machine_modules.add(self)

machine_modules.add(CustomModule("auro\x1fffi\x1ffunction", nil, closure_proc))
