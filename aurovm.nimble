# Package

version       = "0.1.0"
author        = "Arnaud Castellanos Galea"
description   = "The reference implementation of the Auro Virtual Machine"
license       = "MIT"
srcDir        = "src"
bin           = @["aurovm"]



# Dependencies

requires "nim >= 1.2.0", "libffi >= 1.0.5"