#include <stdio.h>

static void *FOURTH;

int first (int (*second)(int), void *fourth) {
    //printf("first(%lld, %lld) size: %d\n", second, fourth, sizeof(int));
    FOURTH = fourth;
    printf("First (C): %d\n", second(42));
    return 1;
}

int third (int arg) {
    int (*fourth)(int) = FOURTH;

    printf("Third (C): %d\n", arg);
    printf("third.fourth() => %d\n", fourth(arg));
    return arg - 1;
}